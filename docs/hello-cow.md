# 🐮 Cow API: Hello World with a Cow

!!! info
    - 🚧 this is a WIP

```javascript
fastify.get('/api/cow/hello', async function (req, res) {
  res
    .header('Content-Type', 'application/json; charset=utf-8')
    .send(cowsay.say({
      text : "👋 Hello World 🌍",
      e : "oO",
      T : "U "
  }));
});
```